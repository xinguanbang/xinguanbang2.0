//获取参数
let gradeParam = location.search.substring(1)
if(gradeParam){
    grades = gradeParam.split('&')
    if(grades[0].split('=')[1] != '' && grades[1].split('=')[1] === ''){
        grade = grades[0].split('=')[1]
    }else if(grades[0].split('=')[1] === '' && grades[1].split('=')[1] != ''){
        grade = grades[1].split('=')[1]
    }
}else{
    grade = 78
}
console.log('分数：'+grade)
$('#Ul1 li:first-child>span:nth-child(2) span:nth-child(2)').text(grade)

//生成数据
//var Ul1 = $('#Ul1')
var arr=[]
for(var j=0;j<100;j++){
    if(j<grade){
        arr.push(1)
    }else{
        arr.push(0)
    } 
}
console.log(arr) //有序数组
var randomNumber =()=>{
    return 0.5 - Math.random()
}
arr.sort(randomNumber)
console.log(arr)  //打乱数组

//根据arr来生成每一个题目
for(var i=1;i<=100;i++){
    var n = i.toString().padStart(3, "0")   //序号
    var x = Math.ceil(Math.random()*99)    //x.toString().padStart(2, "0")
    var y = Math.ceil(Math.random()*99)
    var x1 = x.toString().padStart(2, "0")
    var y1 = y.toString().padStart(2, "0")

    var typecode = Math.random()
    //var res; //正确答案
    if(typecode <= 0.2){ //生成加法运算
        createDate(x+y, "+")
    }else if(typecode <= 0.4 && typecode > 0.2) { //生成减法运算
        createDate(x-y, "-")
    }else if(typecode <= 0.6 && typecode > 0.4) { //生成乘法运算
        createDate(x*y, "*")
    }else if(typecode <= 0.8 && typecode > 0.6) { //生成除法运算
        createDate(x/y, "/")
    }else{ //生成取余运算
        createDate(x%y, "%")
    }

    function createDate(res, type){
        //正确结果就是res
        var res = res.toFixed(2)
        var type = type
        var li = document.createElement('li')
        if(arr[i-1]){     //此处需要生成正确的结果
            li.innerHTML=`<span><span>${n}</span>${x1}&nbsp;${type}&nbsp;${y1}&nbsp;=&nbsp;${res}</span><span>${arr[i-1]}</span>` 
        }else{    //此处需要生成错误的结果
            var falseRes
            do{
                falseRes = Math.floor(Math.random()*9801).toFixed(2) //随机数
            }while(falseRes == res)
            li.innerHTML=`<span><span>${n}</span>${x1}&nbsp;${type}&nbsp;${y1}&nbsp;=&nbsp;${falseRes}</span><span>${arr[i-1]}</span>`
        }
        document.querySelector("#Ul1").appendChild(li)
    }
}

for(var m=1;m<=100;m++){
    var span2 = document.querySelector(`ul li:nth-child(${m+1})>span:nth-child(2)`)
    console.log(span2.innerHTML)//string
    switch (span2.innerHTML) {
        case '0':
            span2.style.color='red'
            break
        case '1':
            span2.style.color='green'
            break 
    } 
}

